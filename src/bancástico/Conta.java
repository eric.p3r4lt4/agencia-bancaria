package bancástico;

public class Conta {
    private Pessoa titular = new Pessoa();
    private int numero;
    private int digito;
    private float saldo;
    
    public Conta(){}
    
    public boolean debita(float v){
        if(this.saldo>=v){
            this.saldo-=v;
            return true;
        }
        else{
            return false;
        }
    }
    
    public void credita(float v){
        this.saldo+=v;
    }

    public Pessoa getTitular() {
        return titular;
    }

    public void setTitular(Pessoa titular) {
        this.titular = titular;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getDigito() {
        return digito;
    }

    public void setDigito(int digito) {
        this.digito = digito;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
    
    @Override
    public String toString(){
        return titular.getNome() + ": R$" + saldo;
    }
}
