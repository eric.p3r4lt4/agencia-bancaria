/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bancástico;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Cliente
 */
public class TelosaController implements Initializable {
    Pessoa Cliente = new Pessoa();
    Conta brad = new Conta();
    
    //Label
    @FXML
    private Label cliente;
    @FXML
    private Label nomeLabel;
    @FXML
    private Label endLabel;
    @FXML
    private Label cidLabel;
    @FXML
    private Label bairroLabel;
    @FXML
    private Label estLabel;
    @FXML
    private Label foneLabel;
    @FXML
    private Label emailLabel;
    @FXML
    private Label cpfLabel;
    @FXML
    private Label rgLabel;
    @FXML
    private Label conta;
    @FXML
    private Label numeroLabel;
    @FXML
    private Label digitoLabel;
    @FXML
    private Label saldoLabel;
    @FXML
    private Label nomeSaldo;
    @FXML
    private Label adicionar;
    @FXML
    private Label retirar;
    @FXML
    private Label resposta;
    
    //TextField
    @FXML
    private TextField nomeText;
    @FXML
    private TextField endText;
    @FXML
    private TextField cidText;
    @FXML
    private TextField bairroText;
    @FXML
    private TextField estText;
    @FXML
    private TextField foneText;
    @FXML
    private TextField emailText;
    @FXML
    private TextField cpfText;
    @FXML
    private TextField rgText;
    @FXML
    private TextField numeroText;
    @FXML
    private TextField digitoText;
    @FXML
    private TextField saldoText;
    @FXML
    private TextField addText;
    @FXML
    private TextField retiText;
    
    //Button
    @FXML
    private Button getCliente;
    @FXML
    private Button getConta;
    @FXML
    private Button getAdd;
    @FXML
    private Button getReti;
    
    
    @FXML
    private void getClienteButton(){
        Cliente.setNome(nomeText.getText());
        Cliente.setEndereço(endText.getText());
        Cliente.setCidade(cidText.getText());
        Cliente.setBairro(bairroText.getText());
        Cliente.setEstado(estText.getText());
        Cliente.setFone(Integer.parseInt(foneText.getText()));
        Cliente.setEmail(emailText.getText());
        Cliente.setCpf(cpfText.getText());
        Cliente.setRg(rgText.getText());
        
        brad.setTitular(Cliente);
        
        nomeText.setEditable(false);
        endText.setEditable(false);
        cidText.setEditable(false);
        bairroText.setEditable(false);
        estText.setEditable(false);
        foneText.setEditable(false);
        emailText.setEditable(false);
        cpfText.setEditable(false);
        rgText.setEditable(false);
        getCliente.setDisable(true);
        
        conta.setVisible(true);
        numeroLabel.setVisible(true);
        numeroText.setVisible(true);
        digitoLabel.setVisible(true);
        digitoText.setVisible(true);
        saldoLabel.setVisible(true);
        saldoText.setVisible(true);
        getConta.setVisible(true);
    }
    
    @FXML
    private void getContaButton(){
        brad.setNumero(Integer.parseInt(numeroText.getText()));
        brad.setDigito(Integer.parseInt(digitoText.getText()));
        brad.setSaldo(Float.parseFloat(saldoText.getText()));
        
        conta.setVisible(false);
        numeroLabel.setVisible(false);
        numeroText.setVisible(false);
        digitoLabel.setVisible(false);
        digitoText.setVisible(false);
        saldoLabel.setVisible(false);
        saldoText.setVisible(false);
        getConta.setVisible(false);
        
        cliente.setVisible(false);
        nomeLabel.setVisible(false);
        endLabel.setVisible(false);
        cidLabel.setVisible(false);
        bairroLabel.setVisible(false);
        estLabel.setVisible(false);
        foneLabel.setVisible(false);
        emailLabel.setVisible(false);
        cpfLabel.setVisible(false);
        rgLabel.setVisible(false);
        nomeText.setVisible(false);
        endText.setVisible(false);
        cidText.setVisible(false);
        bairroText.setVisible(false);
        estText.setVisible(false);
        foneText.setVisible(false);
        emailText.setVisible(false);
        cpfText.setVisible(false);
        rgText.setVisible(false);
        getCliente.setVisible(false);
        
        nomeSaldo.setVisible(true);
        adicionar.setVisible(true);
        retirar.setVisible(true);
        addText.setVisible(true);
        retiText.setVisible(true);
        getAdd.setVisible(true);
        getReti.setVisible(true);
        resposta.setVisible(true);
        
        nomeSaldo.setText(brad.toString());
    }
    
    @FXML
    private void creditaButton(){
        brad.credita(Float.parseFloat(addText.getText()));
        resposta.setText("Adicionado com sucesso!");
        nomeSaldo.setText(brad.toString());
    }
    
    @FXML
    private void debitaButton(){
        if(brad.debita(Float.parseFloat(retiText.getText()))){
            resposta.setText("Operação feita com sucesso!");
        }
        
        else{
            resposta.setText("Você não tem saldo suficiente para retirar essa quantia!");
        }
        
        nomeSaldo.setText(brad.toString());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
